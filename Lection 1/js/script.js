var variable;
// var variable = 'Hello';
var variable2 = 'World';

//Data types
//Number
//String
//Boolean
//Date
//undefined
//Null

// var firstName = 'Bogdana';
// var age = 25;

// var LastName = prompt('What is your last name?');
// console.log('Full Name is: ' + firstName + ' ' + LastName);

// var yourAge = prompt('How old are you?');
// var result;
// if (yourAge < 18) {
//     result = 'You are too young to drive';
// }
// else {
//     result = 'Horaay!!';
// }
// var result = (yourAge < 18 ? "You are too young to drive" : "Horaay!!");
// console.log(result);

var object = {
    firstName: 'John',
    lastName: 'Doe',
    age: 28,
    kids: ['Sara', 'Jessi'],
    getBirthYear: function() {
        return 2018 - this.age;
    }
}

// console.log(object);
// object.firstName = 'Jessy';
// console.log(object.firstName);
// console.log(object.getBirthYear());

// function getBirthYear(age, now = 2018) {
//     return now - age;
// }

// console.log(getBirthYear(20, 2019));

function myFunction() {
    var a = 4;
    return a * a;
}
console.log(a);

// var a = 4;
// function myFunction() {
//     return a * a;
// }