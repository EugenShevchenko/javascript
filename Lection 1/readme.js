// alert('hello');

var hello = 'Hello';

// let hello = 'Hello';
const word = 'Word';

// console.log(qwe);
//Dynamic typing
// Primitive DataTypes
// Number - always floating points
// String
// Boolean
// undefined - automatically assigned to a variable which has no value yet
// Null

//can starts with $variable and _variable

/*
    Multiline comment
*/

// String concatenation - + sign

var firstName = 'John';
var age = 64;

console.log(firstName + ' ' + age);

// var LastName = prompt('What is his last name?');

// console.log('Full name: ' + firstName + ' ' + LastName);

// Operators
//Math: + - / * **
//Logical: > < >= <=
//Typeof:
console.log(typeof firstName);
//Precedance  - порядок операторів
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence

// syntax of:
// if/elіe
// var yourAge = prompt('How old are you?');
// var result;
// if (yourAge < 18) {
//     result = 'You are too young to drive';
// }
// else {
//     result = 'Horaay!!';
// }
// var result = (yourAge < 18 ? "You are too young to drive" : "Horaay!!");
// console.log(result);
// arrays
var names = ['John', 'Sara', 'Connor', 'Arni']
console.log(names);
console.log(names.length);
//array operations
names.push('Last');
names.unshift('First');
names.pop(); // removes last element of array
names.shift(); // removes first element of array
names.indexOf('Sara'); //returns position of element - show -1
delete names[0];
var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.splice(2, 0, "Lemon", "Kiwi");
var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.splice(0, 1);        // Removes the first element of fruits

var fruits = ["Banana", "Orange", "Lemon", "Apple", "Mango"];
var citrus = fruits.slice(1);
//objects
var obj = {
    firstName: 'Jonh',
    lastName: 'Joe',
    birthday: 2000,
    // jobs: ['Gardener', 'Teacher', 'Spy'],
    // getAge: function(){
    //     return 2018 - this.birthday;
    // }
}

console.log(obj);
console.log(obj.birthday);
console.log(obj['birthday']); //this is also allowed
// console.log(obj.getAge()); //this is also allowed


// functions
// function getBirthYear(age, now = 2018) {
//     return now - age;
// }

// console.log(getBirthYear(30));

//visibility rules
function myFunction() {
    var a = 4;
    return a * a;
}

var a = 4;
function myFunction() {
    return a * a;
}
//inside and outside visibility //global and local variable
// closures

var add = (function () {
    var counter = 0;
    return function () { counter += 1; return counter }
})();

add();
console.log(add());
add();


//This is called a JavaScript closure. It makes it possible for a function to have "private" variables.

// objects and "clases" - for later
// loops and iterations
// for(var i = 0; i < 10; i++){
//     console.log(i);
// }

var j = 0;
console.log('length: ' + names.length)
while(j < names.length) {
    if (typeof names[j] == 'string') continue
    console.log(names[j]);
    j++;
}