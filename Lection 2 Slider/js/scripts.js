lastName = "Doe";

// function myFunc(){
//     console.log(this);
// }
// myFunc();

var object = {
    firstName: "Vasiliy",
    lastName: "Pupkin",
    fullName: function () {
        var self = this;
        function innerFunction() {
            console.log(self);
            console.log('Inner');
        }
        innerFunction();
        return this.firstName + ' ' + this.lastName;
    }
}

console.log(object.fullName());