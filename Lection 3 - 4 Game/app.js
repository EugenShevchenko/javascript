/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

//DOM
//Document Object Model
var dice;
var totalScore = [0,0];
var activePlayer = 0;
var currentScore = 0;
var currentNumber = 0;

var isActive;

function initGame() {
    isActive = true;
    activePlayer = 0;
    document.querySelector('.player-0-panel').classList.add('active');
    document.querySelector('.dice').style.display = 'none';
    document.querySelector('#score-0').textContent = totalScore[0];
    document.querySelector('#score-1').textContent = totalScore[1];
    document.querySelector('#current-0').textContent = 0;
    document.querySelector('#current-1').textContent = 0;
}

initGame();

document.querySelector('.btn-roll').addEventListener('click', function() {
    if (isActive) {
        //get random number 1-6
        currentNumber = Math.floor(Math.random() * 6) + 1;
        //reveal dice image
        document.querySelector('.dice').src = 'dice-' + currentNumber + '.png';
        document.querySelector('.dice').style.display = 'block';
        //оновити поточний рахунок
        currentScore += currentNumber;
        document.querySelector('#current-' + activePlayer).textContent = currentScore;
        if (currentNumber === 1) {
            switchTurn();
        }
    //1 result must change turn and nulate counter
    }
});

function switchTurn() {
    //reset current score
    currentScore = 0;
    document.querySelector('#current-' + activePlayer).textContent = currentScore;
    //change turn:
    //change active player
    if (activePlayer == 1) {
        activePlayer = 0;
    } else {
        activePlayer = 1;
    }
    //change visual of active player
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
    //hide dice image
    document.querySelector('.dice').style.display = 'none';
}

//hold function:
function holdEvent() {
    if (isActive) {
        //save currentScore and save it to totalScore
        totalScore[activePlayer] += currentScore;
        //CHECK FOR WIN
        if (totalScore[activePlayer] >= 5) {
            document.querySelector('#score-' + activePlayer).textContent = totalScore[activePlayer];
            document.querySelector('#current-' + activePlayer).textContent = 0;
            isActive = false;
            //if totalScore get's to 21 or more then Player wins
            //prevent roll and hold -- DONE
            //remove active status 
            document.querySelector('.player-0-panel').classList.remove('active');
            document.querySelector('.player-1-panel').classList.remove('active');
            //change Player name for WINNER!!!
            document.querySelector('.player-' + activePlayer + '-panel .player-name').textContent = 'WINNER!!!';
            //add winner class to right player
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');

            //update score

            //hide dice
            document.querySelector('.dice').style.display = 'none';
        }
        else {
            //update TotalScore for user
            // <div class="player-score" id="score-0">0</div>
            document.getElementById('score-' + activePlayer).textContent = totalScore[activePlayer];
            //nulate currentScore and switch turn
            switchTurn();
        }
    }
}
//get hold event
document.getElementById('hold-btn').addEventListener('click', holdEvent);

//new
//all is reset

