//ES5
// var name5 = 'Bogdan';
// console.log(name5);

// console.log(name61);
// const name6 = 'Derry';
// var name61 = 'Anton';

//ES5

// function driverLicense(passedTest) {
//     if (passedTest) {
//         var firstName = 'Bogdan';
//         var yearOfBirth = 1999;
//     }

//     console.log(firstName + ', born in ' + yearOfBirth + ', in allowed to drive a car');
// }

// driverLicense(true);

// function driverLicense6(passedTest) {
//     let firstName = 'Someone';
//     let yearOfBirth = 2000;

//     if (passedTest) {
//         firstName = 'Bogdan';
//         yearOfBirth = 1999;
//     }

//     console.log(firstName + ', born in ' + yearOfBirth + ', in allowed to drive a car');
// }

// driverLicense6(true);

// let i = 23;

// for (i = 0; i < 5; i++) {
//     console.log(i);
// }

// console.log(i);

//ES6

// let i = 15;
// {
//     let i = 10;
//     console.log(i);
// }

// console.log(i);

//Strings

//ES5

// var firstName = 'Bogdan';
// let lastName = "Bond";
// var job = 'student';
// var yearOfBirth = 1999;

// function calcAge(year) {
//     return 2018 - year;
// }

// console.log("My name is " + firstName + " I am " + job + " I was born in " + yearOfBirth + " ich bin " + calcAge(yearOfBirth));

// console.log(`My name is ${firstName} I am ${job} I was born in ${yearOfBirth} ich bin ${calcAge(yearOfBirth)}`);

// const n = `${firstName} ${lastName}`;
// console.log(`${lastName}, ${n}`);
// console.log(n.startsWith('B'));
// console.log(n.endsWith('nd'));
// console.log(n.includes('gda'));
// console.log(`${firstName} `.repeat(10));

//Arrow functions
// const years = [1990, 1965, 1982, 1937];

// //ES5
// var ages5 = years.map(function(el) {
//     return 2018 - el;
// });

// console.log(ages5);

// let ages6 = years.map((el, index) => `Age element #${index + 1} ${2018 - el}`);
// console.log(ages6);

//ES5

// var box5 = {
//     color: 'green',
//     position: 1,
//     clickMe: function() {
//         console.log(this); //this = box5
//         var self = this;
//         document.querySelector('.green').addEventListener('click', function() {
//             console.log(self);
//             var str = 'This is box number ' + self.position + " and it's color is " + self.color;
//             alert(str);
//         });
//     }
// }

// box5.clickMe();

//ES6
// let box6 = {
//     color: 'green',
//     position: 1,
//     clickMe: function () {
//         document.querySelector('.green').addEventListener('click', () => {
//             let str = 'This is box number ' + this.position + " and it's color is " + this.color;
//             alert(str);
//         });
//     }
// }

// box6.clickMe();

// function Person(name) {
//     this.name = name;
// }

/////////////////////////////////
// Lecture: Destructuring

//ES5
// var john = ['John', 28];

// var name = john[0];
// var age = john[1];

//ES6
// let [name, age] = ['John', 28];
// console.log(name);
// console.log(age);

// const obj = {
//     firstName: 'Bogdan',
//     lastName: 'Samofal',
// }

// const { firstName: a, lastName: b } = obj;
// console.log(a);
// console.log(b);

// function calcAgeRetirement(year) {
//     const age = new Date().getFullYear() - year;
//     return [age, 65 - age];
// }

// const [age2, retirement] = calcAgeRetirement(1999);
// console.log(age2);
// console.log(retirement);


//Arrays

// const boxes = document.querySelectorAll('.box');
// console.log(boxes);

// //ES5
// var boxesArr5 = Array.prototype.slice.call(boxes);
// console.log(boxesArr5);
// boxesArr5.forEach(function(current){
//     current.style.backgroundColor = 'red';
// });


// //ES6
// let boxesArr6 = Array.from(boxes);

// Array.from(boxes).forEach(current => current.style.backgroundColor = 'blue');

//ES5
// for(var i = 0; i < boxesArr5.length; i++) {
//     if (boxesArr5[i].className === 'box blue') {
//         continue;
//     }
//     boxesArr5[i].textContent = 'Changed to blue';
// }

//ES6
// for (const current of boxesArr6) {
//     if (current.className.includes('blue')) {
//         continue;
//     }
//     current.textContent = 'Changed to blue 6';
// }


// //Spread operator
// function addFourAges(a, b, c, d) {
//     return a + b + c + d;
// }

// var sum1 = addFourAges(18, 30, 12, 21);
// console.log(sum1);

// //ES5
// var ages = [18, 30, 12, 21];
// var sum2 = addFourAges.apply(null, ages);
// console.log(sum2);

// //ES6
// const sum3 = addFourAges(...ages);
// console.log(sum3);

// const familySmith = ['John', 'Jane', 'Mark'];
// const familyMiller = ['Mary', 'Bob', 'Ann'];
// const bigFamily = [...familySmith, 'Bogdan', ...familyMiller];
// console.log(bigFamily);

// const h = document.querySelector('h1');
// const boxes = document.querySelectorAll('.box');
// const all = [h, ...boxes];

// console.log(all);
// all.forEach(cur => cur.style.color = 'purple');


//Rest parameters

//ES5
// function isFullAge5() {
//     // console.log(arguments);
//     var argsArr = Array.prototype.slice.call(arguments);

//     argsArr.forEach(function (cur) {
//         console.log((2018 - cur) >= 18);
//     })
// }

// isFullAge5(1990, 1999, 1965);
// isFullAge5(1990, 2002, 1965, 2016, 1987);

//ES6
// function isFullAge6(...years) {
//     years.forEach(cur => console.log((2018 - cur) >= 18));
// }


// isFullAge6(1990, 2002, 1965, 2016, 1987);

//ES6
// function isFullAge6(limit, ...years) {
//     years.forEach(cur => console.log((2018 - cur) >= limit));
// }

// isFullAge6(33, 1990, 2006, 1965, 2016, 1987);


/////////////////////////////////
// Lecture: Default parameters

//ES6
// function SmithPerson(firstName, yearOfBirth, lastName = 'Smith', nationality = 'american') {
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.yearOfBirth = yearOfBirth;
//     this.nationality = nationality;
// }


// var john = new SmithPerson('John', 1990);
// var emily = new SmithPerson('Emily', 1983, 'Diaz', 'spanish');


// console.log(john);
// console.log(emily);


/////////////////////////////////
// Lecture: Maps

// const question = new Map();
// question.set('question', 'What is the official name of the latest major JavaScript version?');
// question.set(1, 'ES5');
// question.set(2, 'ES6');
// question.set(3, 'ES2015');
// question.set(4, 'ES7');
// question.set('correct', 3);
// question.set(true, 'Correct answer :D');
// question.set(false, 'Wrong, please try again!');

// console.log(question.get('question'));
// //console.log(question.size);


// if(question.has(4)) {
//     //question.delete(4);
//     //console.log('Answer 4 is here')
// }

// //question.clear();


// //question.forEach((value, key) => console.log(`This is ${key}, and it's set to ${value}`));


// for (let [key, value] of question.entries()) {
//     if (typeof(key) === 'number') {
//         console.log(`Answer ${key}: ${value}`);
//     }
// }

// const ans = parseInt(prompt('Write the correct answer'));
// console.log(question.get(ans === question.get('correct')));

/////////////////////////////////
// Lecture: Classes


// //ES5
// var Person5 = function(name, yearOfBirth, job) {
//     this.name = name;
//     this.yearOfBirth = yearOfBirth;
//     this.job = job;
// }

// Person5.prototype.calculateAge = function() {
//     var age = new Date().getFullYear - this.yearOfBirth;
//     console.log(age);
// }

// // var john5 = new Person5('John', 1990, 'teacher');

// //ES6
// class Person6 {
//     constructor (name, yearOfBirth, job) {
//         this.name = name;
//         this.yearOfBirth = yearOfBirth;
//         this.job = job;
//     }

//     calculateAge() {
//         var age = new Date().getFullYear - this.yearOfBirth;
//         console.log(age);
//     }

//     static greeting() {
//         console.log('Hey there!');
//     }
// }

// const john6 = new Person6('John', 1990, 'teacher');
// console.log(john6);
// Person6.greeting();

/////////////////////////////////
// Lecture: Classes and subclasses

//ES5
var Person5 = function(name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
}

Person5.prototype.calculateAge = function() {
    var age = new Date().getFullYear() - this.yearOfBirth;
    console.log(age);
}

var Athlete5 = function(name, yearOfBirth, job, olymicGames, medals) {
    Person5.call(this, name, yearOfBirth, job);
    this.olymicGames = olymicGames;
    this.medals = medals;
}

Athlete5.prototype = Object.create(Person5.prototype);


Athlete5.prototype.wonMedal = function() {
    this.medals++;
    console.log(this.medals);
}


var johnAthlete5 = new Athlete5('John', 1990, 'swimmer', 3, 10);

johnAthlete5.calculateAge();
johnAthlete5.wonMedal();


//ES6
class Person6 {
    constructor (name, yearOfBirth, job) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.job = job;
    }

    calculateAge() {
        var age = new Date().getFullYear() - this.yearOfBirth;
        console.log(age);
    }
}

class Athlete6 extends Person6 {
    constructor(name, yearOfBirth, job, olympicGames, medals) {
        super(name, yearOfBirth, job);
        this.olympicGames = olympicGames;
        this.medals = medals;
    }
    
    wonMedal() {
        this.medals++;
        console.log(this.medals);
    }
}

const johnAthlete6 = new Athlete6('John', 1990, 'swimmer', 3, 10);

johnAthlete6.wonMedal();
johnAthlete6.calculateAge();











