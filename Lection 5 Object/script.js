// var john = {
//     name: 'John',
//     yearOfBirth: 1999,
//     job: 'teacher'
// };

// /**
//  * COnstructor of Class Person
//  * 
//  * @param {*} name 
//  * @param {*} yearOfBirth 
//  * @param {*} job 
//  */
// var Person = function (name, yearOfBirth, job) {
//     this.name = name;
//     this.yearOfBirth = yearOfBirth;
//     this.job = job;
// }

// Person.prototype.calculateAge = function() {
//     console.log(2018 - this.yearOfBirth);
// }

// Person.prototype.college = 'Zkr';

// var bogdan = new Person('Bogdan', 1999, 'student');
// var nikita = new Person('Nikita', 1999, 'admin');
// var lena = new Person('Lena', 2000, 'artist');

// // console.log(john);
// console.log(bogdan.college);
// console.log(nikita.college);
// console.log(lena);
// console.log(lena.college);
// bogdan.calculateAge();


// Primitives
// var a = 23;
// var b = a;
// a = 46;
// console.log(a);
// console.log(b);

// var obj1 = {
//     name: 'Bogdan',
//     age: 19
// }

// var obj2 = obj1;
// obj1.age = 30;

// console.log(obj1.age);
// console.log(obj2.age);

// var age = 27;
// var obj = {
//     name: 'Jonas',
//     city: 'Lisbon',
// };

// function change(a, b) {
//     a = 30;
//     b.city = "San Francisco";
// }

// change(age, obj);

// console.log(age);
// console.log(obj.city);

///////////////
// Closure
////////

// function retirement(retirementAge) {
//     var a = ' year left until retirement.';
//     return function(yearOfBirth) {
//          var age = 2016 - yearOfBirth;
//          console.log((retirementAge - age) + a);
//     }
// }

// retirement(65)(2000);
// retirement(75)(2000);
// retirement(45)(2000);

// function iterviewQuestion(job) {
//     return function(name) {
//         if (job === 'artist') {
//             console.log(name + ', what kind of braches do you use');
//         } else if (job === "astronaut") {
//             console.log(name + ', Should you drill holes in your space station');
//         } else {
//             console.log('Hello ' + name + ", tell us what do you do?");
//         }
//     }
// }

// iterviewQuestion('astronout')('Lena');

// V8
console.log(variable);
let variable = 'String';
const constant = 3.14;

// constant = 10;
variable = 'Hello';

console.log(constant);
